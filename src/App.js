import './App.css';
import {useState} from 'react'
import './components/restrictedPage'
import RestrictedPage from './components/restrictedPage';

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const user = 'Alex'

  function logIn () {
    setIsLoggedIn(true)
  }

  function logOut () {
    setIsLoggedIn(false)
  }

  return (
    <div className="App">
      <header className="App-header">
        <RestrictedPage isLoggedIn={isLoggedIn} user={user} login={logIn} logout={logOut}/>
      </header>
    </div>
  );
}

export default App;
