import "./styles.css"

function RestrictedPage({ isLoggedIn, user, login, logout }) {
  return (
    <div>
      {isLoggedIn ? (
        <div>
          <h1>Esse é o cara: {user}!</h1>
          <button onClick={logout}>Voltar</button>
        </div>
      ) : (
        <div>
          <h1>Quer saber o nome do maior pescador do Brasil?</h1>
          <button onClick={login}>Clique aqui e conheça</button>
        </div>
      )}
    </div>
  );
}

export default RestrictedPage;
